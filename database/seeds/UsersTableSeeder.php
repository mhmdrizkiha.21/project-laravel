<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => "superadmin",
            'email' => "superadmin@superadmin",
            'password' => Hash::make("12345678"),
            'role' => "superadmin",
            'profile_id' => 1
        ]);
    }
}
