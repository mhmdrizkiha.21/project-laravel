<?php

use App\Profile;
use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profile::create(
            [
                'name' => "super admin",
                'phone_number' => "000000",
                'city_id' => 1
            ]
        );
    }
}
