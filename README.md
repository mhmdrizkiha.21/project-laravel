 
# Final Project #
---
### Kelompok 11 ###
---
### Anggota Kelompok ###
- Naila Shafirni Hidayat
- Muhammad Rizki Halomoan
- Mikhael Kevin
---
### Tema Project ###
E-Sport Tournament Portal 

---
### ERD ###

<img src="ERD.png"/>

---
### Link Video ###
---
Link Demo Aplikasi: [Youtube](https://youtu.be/Kj51nyNLy1w)

Link Deploy: [Heroku](http://jabar-esports.herokuapp.com/)

---
Note: Jalankan seeder untuk melakukan input kota dan users dengan role superadmin. (username: superadmin | password: 12345678)