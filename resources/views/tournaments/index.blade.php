@extends('layouts.master')
@section('judul', 'Tournaments')

@section('content')
<div class="row mb-4  ">
  <div class="col d-flex justify-content-center">
    <h1>LIST OF TOURNAMENTS</h1>
  </div>
</div>
<div class="row">
  @forelse ($tournament as $item)
  <div class="card mx-4" style="width: 18rem;">
    <img src="{{asset('tournament_poster/'. $item->tournament_poster)}}" height="300" width="300" class="img-thumbnail mt-2">
    <div class="card-body">
      <h5 class="card-title">{{$item->tournament_name}}</h5>
      {{-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> --}}
      <p class="card-text">Date: {{$item->tournament_date}}</p>
      <p class="card-text">Prizepool: {{"Rp.". number_format($item->tournament_prizepool)}}</p>
      <a href="/tournament/{{$item->id}}" class="stretched-link"></a>
    </div>
  </div>
  @empty
  <tr>
    <div class="d-flex justify-content-center"> 
      <h3 style="color: grey"><i>There is no tournament yet.</i></h3>  
    </div>  
  </tr>    
  @endforelse
</div>
@endsection
