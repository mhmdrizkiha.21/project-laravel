@extends('layouts.master')
@section('title', 'Create Tournament')
@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Edit Tournament</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="/tournament/{{ $tournament->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="tournament_name">Tournament Name</label>
                <input type="text" class="form-control" name="tournament_name" id="tournament_name" value="{{ $tournament->tournament_name }}">
                @error('tournament_name')
                <div class="alert alert-danger mt-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tournament_description">Description</label>
                <textarea class="form-control" name="tournament_description" rows="10">{{ $tournament->tournament_description }}</textarea>
                @error('tournament_description')
                <div class="alert alert-danger mt-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tournament_rule">Rule</label>
                <textarea class="form-control" name="tournament_rule"  rows="10">{{ $tournament->tournament_rule }}</textarea>
                @error('tournament_rule')
                <div class="alert alert-danger mt-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="row">
                <div class="form-group col-4">
                    <label for="tournament_prizepool">Tournament Prizepool</label>
                    <input type="number" class="form-control" name="tournament_prizepool" id="tournament_prizepool" value="{{ $tournament->tournament_prizepool }}" min="0" placeholder="Masukkan jumlah prizepool">
                    @error('tournament_prizepool')
                        <div class="alert alert-danger mt-1">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group col-4">
                    <label for="tournament_date">Tournament Date</label>
                    <input type="date" class="form-control" name="tournament_date" id="tournament_date" min="0" value="{{ $tournament->tournament_date }}">
                    @error('tournament_date')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label for="tournament_poster">Tournament Poster</label>
                <input type="file" class="form-control" name="tournament_poster" id="tournament_poster">
            </div>
            
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
    <!-- /.card-body -->
  </div>

@endsection