@extends('layouts.master')
@section('judul', 'Create Tournament')
@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Add Tournament</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="/tournament" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="tournament_name">Tournament Name</label>
                <input type="text" class="form-control" name="tournament_name" id="tournament_name" placeholder="Masukkan nama tournament" required>
                @error('tournament_name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tournament_description">Description</label>
                <textarea class="form-control" name="tournament_description" rows="10"></textarea>
                @error('tournament_description')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tournament_rule">Rule</label>
                <textarea class="form-control" name="tournament_rule" rows="10"></textarea>
                @error('tournament_rule')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="row">
                <div class="form-group col-4">
                    <label for="tournament_prizepool">Tournament Prizepool</label>
                    <input type="number" class="form-control" name="tournament_prizepool" id="tournament_prizepool" min="0" placeholder="Masukkan jumlah prizepool">
                    @error('tournament_prizepool')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group col-4">
                    <label for="tournament_date">Tournament Date</label>
                    <input type="date" class="form-control" name="tournament_date" id="tournament_date" min="0"">
                    @error('tournament_date')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label for="tournament_poster">Tournament Poster</label>
                <input type="file" class="form-control" name="tournament_poster" id="tournament_poster">
            </div>
            @error('tournament_poster')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
    <!-- /.card-body -->
  </div>

@endsection