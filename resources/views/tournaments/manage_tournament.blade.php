@extends('layouts.master')
@section('judul','Manage Tournament')
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush
@push('script')
    <script src="{{ asset('vendors/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(document).ready( function () {
            $('#tournamentTbl').DataTable();
        } );
    </script>
@endpush

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Manage Tournament</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered" id="tournamentTbl">
            <thead class="bg-light">
                <tr>
                    <th>#</th>
                    <th>Tournament Name</th>
                    <th>Description</th>
                    <th>Rule</th>
                    <th>Date</th>
                    <th>Prizepool</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($tournament as $key=>$item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{Str::limit($item->tournament_name, 30)}}</td>
                        <td>{{Str::limit($item->tournament_description, 15)}}</td>
                        <td>{{Str::limit($item->tournament_rule, 10)}}</td>
                        <td>{{ $item->tournament_date }}</td>
                        <td>{{ "Rp." . number_format($item->tournament_prizepool,2)  }}</td>
                        <td>
                            <a href="/tournament/{{ $item->id }}/edit" class="badge btn-warning">Edit</a>
                            <a href="/manage-player/{{ $item->id }}" class="badge btn-primary">Manage Player</a><br><br>
                            <form action="/tournament/{{$item->id}}" method="POST" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="badge btn-danger" >Delete</button>
                            </form>
                        </td>
                    </tr>
                @empty
                <tr>
                    <td colspan="7">
                        <div class="d-flex justify-content-center">
                                <h4 style="color: grey">Opsss, there is no tournament yet.
                                <a href="/tournament/create">Click here</a> to add some.</h4>
                        </div>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
  </div>
    
@endsection

