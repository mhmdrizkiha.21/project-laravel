@extends('layouts.master')
@section('judul','Manage Player')
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush
@push('script')
    <script src="{{ asset('vendors/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(document).ready( function () {
            $('#tournamentTbl').DataTable();
        } );
    </script>
@endpush

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Manage Tournament | </h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if ($tournament->first() != null)
            <a href="/tournament/{{$tournament->first()->tournament_id}}/cetak_pdf" class="btn btn-primary btn-sm mb-3">Cetak PDF</a>            
        @endif
        <table class="table table-bordered" id="tournamentTbl">
            <thead class="bg-light">
                <tr>
                    <th class="">#</th>
                    <th style="text-align: center">PLAYER</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($tournament as $key=>$item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td >{{ $item->users->username }}
                            <form action="/manage-player/{{ $item->tournament_id }}/{{$item->user_id}}" method="POST" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="badge btn-danger" >Kick Player</button>
                            </form>
                        </td>
                        
                    </tr>
                @empty
                <tr>
                    <td colspan="2">
                        <div class="d-flex justify-content-center">
                            <h4 style="color: grey"><i>There is no player registered yet.</i></h4>
                        </div>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
  </div>
    
@endsection

