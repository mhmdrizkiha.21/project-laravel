<!DOCTYPE html>
<html>
<head>
	<title>Daftar Peserta {{$tournament_user->first()->tournament->tournament_name}}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container-fluid">
        <h4 class="text-center">Daftar Peserta {{$tournament_user->first()->tournament->tournament_name}}</h4>
		<br>
		<table class='table table-bordered'>
			<thead>
				<tr>
					<th>#</th>
					<th>Username</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone Number</th>
					<th>City</th>
				</tr>
			</thead>
			<tbody>
                @forelse ($tournament_user as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->users->username}}</td>
                        <td>{{$item->users->profile->name}}</td>
                        <td>{{$item->users->email}}</td>
                        <td>{{$item->users->profile->phone_number}}</td>
                        <td>{{$item->users->profile->city->city_name}}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">Tidak ada peserta</td>
                    </tr>
                @endforelse
			</tbody>
		</table>
	</div>
</body>
</html>