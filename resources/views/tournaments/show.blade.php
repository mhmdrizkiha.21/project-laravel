@extends('layouts.master')
@section('judul','Detail Tournament')
@section('content')
<div class="card">

    <div class="card-header">
      {{ $tournament->tournament_name }}
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-2">
                <img src="{{asset('tournament_poster/'. $tournament->tournament_poster)}}" height="300px" width="300px" class="img-thumbnail">
            </div>
            <div class="col-4">
                <p>Tournament Description</p>
                {{ $tournament->tournament_description }}
            </div>
            <div class="col-4">
                <p>Tournament Rule</p>
                {{ $tournament->tournament_rule }}
            </div>
            @if (Auth::user()->role == "player")
            <div class="col-2 justify-content-end">
              <form action="/tournament/{{$tournament->id}}/join" method="POST">
                @csrf
                @if (!$checker)
                  <button type="submit" class="btn btn-primary">Register</button>
              </form>
              @else
                  <a class="btn btn-success">Success</a>
              @endif
          </div>    
            @endif
            
        </div>
        <div class="row mt-4">
            <div class="col-2">
                <p>Date</p>
                {{ $tournament->tournament_date }}
            </div>
            <div class="col-4">
                <p>Total Participants</p>
                {{ count($participants) }}
                <a href="#" data-toggle="modal" data-target="#participants">Show more</a>
            </div>
            <div class="col-4">
                <p>Prizepool</p>
                {{"Rp.". number_format($tournament->tournament_prizepool)}}
            </div>
        </div>
    </div>
  </div>
  
  <!-- Modal -->
<div class="modal fade" id="participants" tabindex="-1" role="dialog" aria-labelledby="participantsLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="participantsLabel">Registered Participants</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php
                $i=0;
            ?>
           <table class="table table-bordered">
              <thead>
                  <tr>
                      <th colspan="5" style="text-align:center" >USERNAME</th>
                  </tr>
              </thead>
              <tbody>
                  @forelse ($participants as $item)
                    @if ($i % 5 == 0)
                    @if ($i)
                        </tr> 
                    @endif
                        <tr> 
                    @endif
                    <td>
                        {{ $item->users->username }}
                    </td> 
                    @php
                        ++$i;
                    @endphp
                  @empty
                    <tr>
                        <td colspan="5" style="text-align:center"><i>There is no participants registered yet!</i></td>
                    </tr>
                  @endforelse
                    </tr>
                </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endpush