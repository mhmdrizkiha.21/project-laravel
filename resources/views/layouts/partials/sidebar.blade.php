<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item nav-category">News</li>
      <li class="nav-item">
        <a class="nav-link" href="/">
          <i class="mdi mdi-newspaper menu-icon"></i>
          <span class="menu-title">News</span>
        </a>
      </li>
      
        @if (Auth::user() && Auth::user()->role != "player")
        <li class="nav-item">
          <a class="nav-link" data-bs-toggle="collapse" data-bs-target="#manageNews">
            <i class="mdi mdi-newspaper menu-icon"></i>
            <span class="menu-title">News</span>
            <i class="menu-arrow"></i>
          </a>
        <div class="collapse" id="manageNews">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="/news">Manage</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/news/create">Add News</a>
            </li>
          </ul>
        </div>    
        @endif
      </li>
      <li class="nav-item nav-category">Tournaments</li>
      <li class="nav-item">
        <a class="nav-link" href="/tournament">
          <i class="menu-icon mdi mdi-trophy"></i>
          <span class="menu-title" >Tournaments</span>
        </a>
      </li>
      @if (Auth::user() && Auth::user()->role != "player")
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" data-bs-target="#manageTournament">
          <i class="menu-icon mdi mdi-trophy"></i>
          <span class="menu-title">Tournaments</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="manageTournament">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="/manage-tournament">Manage</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/tournament/create">Add Tournament</a>
            </li>
          </ul>
        </div>
      </li>
      @endif
      @if (Auth::user() && Auth::user()->role == "superadmin")
      <li class="nav-item nav-category">City</li>
      <li class="nav-item">
        <a class="nav-link" href="/city">
          <i class="mdi mdi-city menu-icon"></i>
          <span class="menu-title">Cities</span>
        </a>
      </li> 
      <li class="nav-item nav-category">User</li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" data-bs-target="#manageUser">
          <i class="menu-icon mdi mdi-account-settings"></i>
          <span class="menu-title">User</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="manageUser">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="\users">Manage</a>
            </li>
          </ul>
        </div>
      </li>   
      @endif
      
    </ul>
  </nav>
  <!-- partial -->