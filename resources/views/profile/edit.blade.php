@extends('layouts.master')
@section('judul')
    Edit Profile
@endsection

@section('content')
<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    {{-- Username
    Nama
    Email
    Phone number
    City --}}
    @method('put')
    <div class="form-group">
        <label for="username">Username</label>
        <input type="text" value="{{$profile->users->username}}" class="form-control" name="username" id="username" disabled>
    </div>
    @error('username')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" value="{{$profile->users->email}}" class="form-control" name="email" id="email" disabled>
    </div>
    @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="inputNama">Nama</label>
        <input type="text" value="{{$profile->name}}" class="form-control" name="name" id="inputNama">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="phone_number">Phone Number</label>
        <input type="text" value="{{$profile->phone_number}}" class="form-control" name="phone_number" id="phone_number">
    </div>
    @error('phone_number')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="city">City</label> <br>
        <select name="city_id" class="form-control custom-select">
            <option value="">--Pilih Kota--</option>
            @foreach ($city as $item)
                @if ($item->id === $profile->city_id)
                    <option value="{{$item->id}}" selected>{{$item->city_name}}</option>                    
                @else
                    <option value="{{$item->id}}">{{$item->city_name}}</option>                                        
                @endif
            @endforeach
        </select>
    </div>
    @error('city_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
