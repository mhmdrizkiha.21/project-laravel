@extends('layouts.master')
@section('judul')
    Detail Profile
@endsection

@section('content')
    
    {{-- Username
    Nama
    Email
    Phone number
    City --}}
    <div class="row">
        <div class="col-3">
            <p class="font-weight-bold">Username</p>
            <p class="font-weight-bold">Nama</p>
            <p class="font-weight-bold">Email</p>
            <p class="font-weight-bold">Phone Number</p>
            <p class="font-weight-bold">City</p>
        </div>
        <div class="col-9">
            <p class="font-weight-normal">{{$profile->users->username}}</p>
            <p class="font-weight-normal">{{$profile->name}}</p>
            <p class="font-weight-normal">{{$profile->users->email}}</p>
                @if($profile->phone_number == NULL || $profile->phone_number == '')
                    <p class="font-weight-normal">-</p>
                @else
                    <p class="font-weight-normal">{{$profile->phone_number}}</p>
                @endif
            <p class="font-weight-normal">{{$profile->city->city_name}}</p>
        </div>
    </div>
    <div class="d-flex justify-content-start mt-2 ">
        <a class="btn btn-info" href="/profile/{{$profile->id}}/edit">Edit Profile</a>
    </div>
@endsection