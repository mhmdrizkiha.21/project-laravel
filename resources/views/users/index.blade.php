@extends('layouts.master')
@section('judul')
    Data Users
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush
@push('script')
    <script src="{{ asset('vendors/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(document).ready( function () {
            $('#tournamentTbl').DataTable();
        } );
    </script>
@endpush
@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Manage Users</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
      <table class="table table-bordered" id="tournamentTbl">
          <thead class="bg-light">
              <tr>
                  <th>#</th>
                  <th>Username</th>
                  <th>Name</th>
                  <th>Role</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
            @forelse ($users as $key=>$item)
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$item->username}}</td>
                <td>{{$item->profile->name}}</td>
                <td>{{$item->role}}</td>
                <td>
                    @if ($item->role == "player")
                    <form action="/users/{{$item->id}}" method="POST" class="d-inline">
                        @csrf
                        @method('PUT')
                        <button type="submit" class="badge btn-warning" >SetAdmin</button>
                    </form>
                    @endif
                    <form action="/users/{{$item->id}}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="badge btn-danger" >Delete</button>
                    </form>
                </td>
              </tr>
            @empty
              <tr>
                  <td colspan="7">
                      <div class="d-flex justify-content-center">
                              <h4 style="color: grey">Opsss, there is no user yet.
                      </div>
                  </td>
              </tr>
              @endforelse
          </tbody>
      </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection