@extends('layouts.master')
@section('title', 'Edit Data')
@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Form News</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="/news/{{$news->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Masukkan Judul" value="{{$news->title}}">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" rows="10">{{$news->description}}</textarea>
                @error('description')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
    <!-- /.card-body -->
  </div>

@endsection