@extends('layouts.master')
@section('title', 'Create Data')
@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Form News</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="/news" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Masukkan Judul">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" rows="10"></textarea>
                @error('description')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
    <!-- /.card-body -->
  </div>

@endsection