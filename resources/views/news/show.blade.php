@extends('layouts.master')
@section('content')
<div class="card">
  <div class="card-body">
    <h5 class="card-title">{{$news->title}} </h5>
    Author: {{$news->users->username}}
    {{-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> --}}
    <p class="card-text">{{$news->description}}</p>
  </div>
</div>
@endsection