@extends('layouts.master')
@section('judul', 'News')

@section('content')
<div class="row">
  @forelse ($news as $value)
  <div class="card mx-3" style="width: 18rem;">
    <div class="card-body">
      <h5 class="card-title">{{$value->title}}</h5>
      {{-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> --}}
      <p class="card-text">{{Str::limit($value->description, 50)}}</p>
      <a href="/news/{{$value->id}}" class="text-decoration-none badge btn-info">Detail</a>
      <a href="/news/{{$value->id}}/edit" class="text-decoration-none badge btn-warning">Edit</a>
      <form action="/news/{{$value->id}}" method="POST" class="d-inline">
        @csrf
        @method('DELETE')
        <button type="submit" class="badge btn-danger" >Delete</button>
      </form>
    </div>
  </div>
  @empty
  <div class="d-flex justify-content-center mt-4">
    <i><h3 style="color: grey">There is no news yet.</h3></i>
  </div>   
  @endforelse
</div>
@endsection
