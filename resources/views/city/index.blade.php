@extends('layouts.master')
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush

@section('judul')
    Data Kota
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h2>Daftar Kota</h2>
    </div>
    <div class="card-body">
        <form class="form-inline" action="/city" method="POST">
            @csrf
            <label for="inputKota" class="mb-2">Tambahkan kota</label>
            <div class="row">
                <div class="col-4">
                    <div class="form-group sm-3 mb-4">
                        <input type="text" class="form-control" id="inputkota" name="city_name" placeholder="Masukkan nama kota">
                    </div>
                </div>
                <div class="col-2">
                    <button type="submit" class="btn btn-sm btn-primary mb-4">Tambah</button>
                </div>
            </div>
            @error('city_name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </form>
        
        <table class="table table-bordered" id="citiesTable">
            <thead class="bg-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Kota</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($city as $key=>$item)
                <tr>
                    <th scope="row">{{$key+1}}</th>
                    <td>{{$item->city_name}}</td>
                    <td>
                        <form action="/city/{{$item->id}}" method="POST">
                            @csrf
                            <a href="/city/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            @method('delete')
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                  </tr>
                @empty
                    <tr>
                        <td colspan="3" align="center">Tidak ada kota</td>
                    </tr>
                @endforelse
            </tbody>
          </table>
    </div>
  </div>
@endsection

@push('script')
    <script src="{{ asset('vendors/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(document).ready( function () {
            $('#citiesTable').DataTable();
        } );
        $('#citiesTable').dataTable({
            bAutoWidth: false, 
            aoColumns : [
                { sWidth: '5%' },
                { sWidth: '50%' },
                { sWidth: '40%' }
            ]
        });
    </script>
@endpush