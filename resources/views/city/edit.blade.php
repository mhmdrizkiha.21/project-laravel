@extends('layouts.master')
@section('judul')
    Edit Data Kota
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h3 class="my-4">Edit Data Kota</h3>
        <form action="/city/{{$city->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="nama">Nama Kota</label>
                <input type="text" id="nama" class="form-control" name="city_name" value="{{$city->city_name}}">
            </div>
            @error('city_name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <button type="submit" class="btn btn-primary mb-2">Edit</button>
        </form>
    </div>
  </div>
@endsection