@extends('layouts.master')
@section('judul', 'News')

@section('content')
<div class="d-flex justify-content-center">
  <h1>NEWS</h1>
</div>
<div class="row">
  @forelse ($news as $value)
  <div class="card col-12 my-3">
    <div class="card-body">
      <h5 class="card-title">{{$value->title}} </h5>
      Author: {{$value->users->username}}
      {{-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> --}}
      <p class="card-text">{{$value->description}}</p>
    </div>
  </div>
  @empty
  <div class="d-flex justify-content-center mt-4">
    <i><h3 style="color: grey">There is no news yet.</h3></i>
  </div>
  @endforelse
</div>
@endsection
