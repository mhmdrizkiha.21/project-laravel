<?php

use App\Http\Controllers\TournamentController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::group(['middleware' => ['auth', 'role:admin|superadmin']], function () {
    /* CRUD FOR NEWS */
    Route::resource('news', 'NewsController');
    /* CRUD FOR TOURNAMENT */
    Route::get('/manage-tournament', 'TournamentController@manageTournament');
    Route::get('/manage-player/{tournament_id}', 'TournamentController@managePlayer');
    Route::delete('/manage-player/{tournament_id}/{user_id}', 'TournamentController@kickPlayer');
    Route::get('/tournament/{tournament_id}/cetak_pdf', 'TournamentController@cetakPdf');
});

//tournament list
Route::resource('tournament', 'TournamentController')->only('index');

Route::group(['middleware' => ['auth', 'role:admin|superadmin']], function () {
    Route::resource('tournament', 'TournamentController')->except('index', 'show');
});

Route::group(['middleware' => ['auth', 'role:player']], function () {
    Route::resource('tournament', 'TournamentController')->only('show');
    Route::post('/tournament/{tournament_id}/join', 'TournamentController@joinTournament');
});

Route::group(['middleware' => ['auth', 'role:player|superadmin|admin']], function () {
    Route::resource('tournament', 'TournamentController')->only('show');
});

Route::group(['middleware' => ['auth', 'role:superadmin']], function () {
    // Cities CRUD
    Route::resource('city', 'CityController');
    // Manage user
    Route::resource('users', 'UserController')->only(
        ['index', 'update', 'destroy']
    );
});


// Profile
Route::resource('profile', 'ProfileController')->only([
    'index', 'edit', 'update'
])->middleware('auth');