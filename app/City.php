<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'citys';
    protected $fillable = ['city_name'];

    public function profile() {
        return $this->hasMany('App\Profile');
    }
}
