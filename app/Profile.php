<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $fillable = ['name', 'phone_number', 'city_id'];

    public function city()
    {
        return $this->belongsTo('App\City');
    }
    public function users()
    {
        return $this->hasOne('App\User');
    }
}
