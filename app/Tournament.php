<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    protected $fillable = [
        'tournament_name', 'tournament_description',
        'tournament_rule', 'tournament_prizepool',
        'tournament_date', 'tournament_poster'
    ];

    public function tournament_user()
    {
        return $this->hasMany('App\Tournament_User');
    }
}
