<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament_User extends Model
{
    protected $table = 'tournament_user';
    protected $fillable = ['user_id', 'tournament_id'];

    public function tournament()
    {
        return $this->belongsTo('App\Tournament', 'tournament_id');
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
