<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::where('role', '!=', 'superadmin')->get();
        return view('users.index', compact('users'));
    }
    public function update($id)
    {
        User::where('id', $id)
            ->update([
                'role' => "admin",
            ]);
        return redirect('/users');
    }
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        Profile::where('id', $user->profile_id)->delete();
        return redirect('/users');
    }
}
