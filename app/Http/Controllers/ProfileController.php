<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\City;
use App\User;
use App\Profile;

class ProfileController extends Controller
{
    public function index()
    {
        // $id = Auth::id;
        $id = Auth::user()->profile_id;  // session user
        $profile = Profile::where('id', $id)->first();
        return view('profile.index', compact('profile'));
    }
    public function edit($id)
    {
        $profile = Profile::where('id', $id)->first();
        $city = City::all();

        return view('profile.edit', compact('profile', 'city'));
    }
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name' => 'required',
                'phone_number' => 'required',
                'city_id' => 'required',
            ],
            [
                'name.required' => 'Nama harus diisi',
                'phone_number.required' => 'Nomor handphone harus diisi',
                'city_id.required' => 'Nama kota harus diisi',
            ]
        );

        $profile = Profile::find($id);

        $profile->name = $request->name;
        $profile->phone_number = $request->phone_number;
        $profile->city_id = $request->city_id;
        $profile->save();

        return redirect('/profile')->with('toast_success','Profil berhasil diubah!');
    }
}
