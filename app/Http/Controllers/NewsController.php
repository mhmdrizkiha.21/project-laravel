<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Illuminate\Support\Facades\Auth;


class NewsController extends Controller
{
    public function index(Request $request)
    {
        $news = News::all();
        return view('news.index', compact('news'));
    }
    public function create()
    {
        return view('news.create');
    }
    public function store(Request $request)
    {
        $user_id = Auth::user()->id; // nanti di isi session dari user
        $request->validate([
            'title' => 'required',
        ]);

        News::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' =>  $user_id
        ]);
        return redirect('/news')->with('toast_success','News berhasil ditambahkan!');
    }
    public function show($id)
    {
        $news = News::where('id', $id)->first();
        return view('news.show', compact('news'));
    }
    public function edit($id)
    {
        $news = News::where('id', $id)->first();
        return view('news.edit', compact('news'));
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);
        News::where('id', $id)
            ->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);
        return redirect('/news')->with('toast_success','News berhasil diupdate!');
    }
    public function destroy($id)
    {
        News::where('id', $id)->delete();
        return redirect('/news')->with('toast_success','News berhasil dihapus!');
    }
}
