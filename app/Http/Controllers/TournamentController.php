<?php

namespace App\Http\Controllers;

use App\Tournament;
use App\Tournament_User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Barryvdh\DomPDF\Facade\Pdf;


class TournamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tournament = Tournament::all();
        return view('tournaments.index', compact('tournament'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tournaments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'tournament_name' => 'required',
                'tournament_prizepool' => 'required',
                'tournament_date' => 'after:now',
                'tournament_poster' => 'required|mimes:jpeg,jpg,png'
            ],
            [
                'tournament_name.required' => 'Nama tournament harus di isi',
                'tournament_prizepool.required' => 'Jumlah prizepool harus di isi',
                'tournament_date.after' => 'Tanggal tournament invallid',
                'tournament_poster.required' => 'Poster harus diisi',
                'tournament_poster.mimes' => 'Poster hanya bisa dalam format jpeg, jpg, atau png'
            ]
        );

        $gambar = $request->tournament_poster;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();

        Tournament::create([
            'tournament_name' => $request->tournament_name,
            'tournament_description' => $request->tournament_description,
            'tournament_rule' => $request->tournament_rule,
            'tournament_prizepool' => $request->tournament_prizepool,
            'tournament_date' => $request->tournament_date,
            'tournament_poster' => $new_gambar
        ]);

        $gambar->move('tournament_poster', $new_gambar);

        return redirect('/manage-tournament')->withSuccess('Tournament berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tournament = Tournament::where('id', $id)->first();
        $participants = Tournament_User::where('tournament_id', $id)->get();
        $checker = Tournament_User::where([
            'tournament_id' => $id,
            'user_id' => Auth::user()->id
        ])->first();
        return view('tournaments.show', compact('tournament', 'participants', 'checker'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tournament = Tournament::where('id', $id)->first();
        return view('tournaments.edit', compact('tournament'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'tournament_name' => 'required',
                'tournament_prizepool' => 'required',
                'tournament_date' => 'after:now',
                'tournament_poster' => 'mimes:jpeg,jpg,png'
            ],
            [
                'tournament_name.required' => 'Nama tournament harus di isi',
                'tournament_prizepool.required' => 'Jumlah prizepool harus di isi',
                'tournament_date.after' => 'Tanggal tournament invallid',
                'tournament_poster.mimes' => 'Poster hanya bisa dalam format jpeg, jpg, atau png'
            ]
        );

        if ($request->has('tournament_poster')) {
            $tournament = Tournament::find($id);
            File::delete("tournament_poster/" . $tournament->tournament_poster);

            $gambar = $request->tournament_poster;
            $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            $gambar->move('tournament_poster', $new_gambar);

            Tournament::where('id', $id)->update([
                'tournament_poster' => $new_gambar
            ]);
        }

        Tournament::where('id', $id)
            ->update([
                'tournament_name' => $request->tournament_name,
                'tournament_description' => $request->tournament_description,
                'tournament_rule' => $request->tournament_rule,
                'tournament_prizepool' => $request->tournament_prizepool,
                'tournament_date' => $request->tournament_date
            ]);

        return redirect('/manage-tournament')->withSuccess('Informasi tournament berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $getPoster = Tournament::find($id);
        File::delete("tournament_poster/" . $getPoster->tournament_poster);
        $tournament = Tournament::where('id', $id)->delete();
        return redirect('/manage-tournament');
    }

    public function manageTournament()
    {
        $tournament = Tournament::get();
        return view('tournaments.manage_tournament', compact('tournament'));
    }

    public function managePlayer($tournament_id)
    {
        $tournament = Tournament_User::where('tournament_id', $tournament_id)->get();
        return view('tournaments.manage_player', compact('tournament'));
    }

    public function kickPlayer($tournament_id, $user_id)
    {
        $kick = Tournament_User::where('tournament_id', $tournament_id)
            ->where('user_id', $user_id)
            ->delete();
        return redirect('/manage-player/' . $tournament_id)->with('toast_success', 'Player berhasil dikeluarkan dari tournament');
    }

    public function joinTournament($id)
    {
        Tournament_User::create(
            [
                'user_id' => Auth::user()->id,
                'tournament_id' => $id
            ]
        );
        return redirect()->to('/tournament/' . $id);
    }

    public function cetakPdf($tournament_id)
    {
        $tournament_user = Tournament_User::where('tournament_id', $tournament_id)->get();
        $tournament_name = Tournament::where('id', $tournament_id)->first()->tournament_name;
        $pdf = PDF::loadview('tournaments.peserta_pdf', ['tournament_user' => $tournament_user]);
        return $pdf->stream();
    }
}
