<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{
    public function index() {
        $city = City::all();
        return view('city.index', compact('city'));
    }
    public function store(Request $request) {
        $request->validate([
            'city_name' => 'required'
        ], 
        [
            'city_name.required' => 'Nama Kota harus diisi'
        ]);

        $city = new City;
        $city->city_name = $request->city_name;
        $city->save();

        return redirect('/city')->withSuccess('Kota berhasil ditambahkan!');
    }
    public function edit($id) {
        $city = City::findOrFail($id);

        return view('city.edit', compact('city'));
    }
    public function update(Request $request, $id) {
        $request->validate([
            'city_name' => 'required'
        ], 
        [
            'city_name.required' => 'Nama Kota harus diisi'
        ]);

        $city = City::find($id);

        $city->city_name = $request->city_name;
        $city->save();

        return redirect('/city')->withSuccess('Kota berhasil diedit!');
    }
    public function destroy($id) {
        City::findOrFail($id)->delete();
        return redirect('/city');
    }
}
